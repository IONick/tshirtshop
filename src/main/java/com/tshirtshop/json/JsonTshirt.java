package com.tshirtshop.json;


import org.json.JSONObject;

public class JsonTshirt {

    private String url;
    private JSONObject jsonObject;

    public JsonTshirt(String json) {
        jsonObject = new JSONObject(json);
    }

    public String getUrl() {
        return jsonObject.getJSONObject("data").getString("url");
    }

    public void setJson(String json) {
        jsonObject = new JSONObject(json);
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }

}

package com.tshirtshop.repositories;

import com.tshirtshop.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> findAllByName(String name);
    Tag findOneByName(String name);
}

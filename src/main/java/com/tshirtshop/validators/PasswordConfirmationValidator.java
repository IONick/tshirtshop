package com.tshirtshop.validators;

import com.tshirtshop.entities.User;
import org.springframework.stereotype.Service;

@Service
public class PasswordConfirmationValidator {
    private User user;
    private String passwordConfirmation;

    public PasswordConfirmationValidator() {
    }

    public PasswordConfirmationValidator(User user, String passwordConfirmation) {
        this.user = user;
        this.passwordConfirmation = passwordConfirmation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public boolean isValidPasswordConfirmation(){
        if(user == null || passwordConfirmation == null){
            return false;
        }
        return passwordConfirmation.equals(user.getPassword());
    }
}

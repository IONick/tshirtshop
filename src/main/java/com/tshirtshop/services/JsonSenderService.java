package com.tshirtshop.services;


import com.tshirtshop.entities.Tshirt;
import com.tshirtshop.json.JsonTshirt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class JsonSenderService {
    @Value("${upload.path}")
    private String path;

    public JsonSenderService() {
    }

    public JsonTshirt sendTshirt(Tshirt tshirt, String base64){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("image", base64);
        map.add("name", tshirt.getName());
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity( path, request , String.class);
        return new JsonTshirt(response.getBody());
    }
}

package com.tshirtshop.services;

import com.tshirtshop.entities.ConfirmationToken;
import com.tshirtshop.entities.Tshirt;
import com.tshirtshop.entities.User;
import com.tshirtshop.models.Role;
import com.tshirtshop.models.Theme;
import com.tshirtshop.repositories.ConfirmationTokenRepository;
import com.tshirtshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;
    @Autowired
    private EmailMessageWriterService emailMessageWriter;
    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private PasswordEncoder passwordEncoder;


    public List<User> getAll(){
        return userRepository.findAll();
    }

    public boolean addUser(User user){
        User userFromDB = userRepository.findByUsername(user.getUsername());
        if(userFromDB != null){
           return false;
        }
        user.setActive(false);
        user.setRole(Role.USER);
        user.setTheme(Theme.LIGHT);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        confirmationToken.setActual(true);
        confirmationTokenRepository.save(confirmationToken);
        MimeMessagePreparator simpleMailMessage = emailMessageWriter.getConfirmMessage(user, confirmationToken);
        emailSenderService.sendEmail(simpleMailMessage);
        return true;
    }

    public boolean checkUsernameExists(String username){
        return userRepository.existsByUsername(username);
    }

    public boolean checkEmailExists(String email){
        return userRepository.existsByEmail(email);
    }

    public User activateUser(String confirmationToken){
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
        if(token == null || !token.getActual()){
            return null;
        }
        User user = token.getUser();
        if(user == null){
            return null;
        }
        user.setActive(true);
        token.setActual(false);
        confirmationTokenRepository.save(token);
        userRepository.save(user);
        return user;
    }

    public void changeUsers(List<User> users, String operation){
        if(operation.equals("delete")){
            for(User user : users){
                List<Tshirt> tshirts = user.getTshirts();
                for(Tshirt tshirt : tshirts){
                    tshirt.setUser(null);
                }
                user.setTshirts(null);
            }
            userRepository.deleteAll(users);
            return;
        }
        List<User> changedUsers = new ArrayList<>();
        for(User user : users){
            switch (operation){
                case "block":{
                    user.setActive(false);
                    changedUsers.add(user);
                    break;
                }
                case "unblock":{
                    user.setActive(true);
                    changedUsers.add(user);
                    break;
                }
                case "set_admin":{
                    user.setRole(Role.ADMIN);
                    changedUsers.add(user);
                    break;
                }
                case "set_user":{
                    user.setRole(Role.USER);
                    changedUsers.add(user);
                    break;
                }
            }
        }
        userRepository.saveAll(changedUsers);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("User is not authorized.");
        }
        return user;
    }

    public User getCurrentUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        User user = userRepository.findByUsername(username);
        return user;
    }

    public void logoutUser(){
        SecurityContextHolder.clearContext();
    }

    public void authenticateUser(User user){
        Authentication auth =
                new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

}

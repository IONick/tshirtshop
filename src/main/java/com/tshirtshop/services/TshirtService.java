package com.tshirtshop.services;

import com.tshirtshop.entities.Tag;
import com.tshirtshop.entities.Tshirt;
import com.tshirtshop.entities.User;
import com.tshirtshop.json.JsonTshirt;
import com.tshirtshop.repositories.TagRepository;
import com.tshirtshop.repositories.TshirtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TshirtService {
    @Autowired
    private TshirtRepository tshirtRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private JsonSenderService jsonSenderService;


    public List<Tshirt> getAll(){
        return tshirtRepository.findAll();
    }

    public boolean hasTags(String tags){
        String regex = "#[a-zA-Z]+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(tags);
        return matcher.find();
    }


    public boolean uploadTshirt(MultipartFile image, Tshirt tshirt, String tags, User user) throws IOException {
        byte[] fileContent = image.getBytes();
        String encodedImage = Base64.getEncoder().encodeToString(fileContent);
        JsonTshirt jsonTshirt = jsonSenderService.sendTshirt(tshirt, encodedImage);
        String regex = "#[a-zA-Z]+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(tags);
        Set<String> tagNameSet = new HashSet<>();
        while (matcher.find()){
            tagNameSet.add(matcher.group().substring(1));
        }
        Set<Tag> tagSet = new HashSet<>();
        for(String  tagName : tagNameSet){
            Tag tagEntity = tagRepository.findOneByName(tagName);
            if(tagEntity == null) {
                Tag tag = new Tag();
                tag.setName(tagName);
                tagRepository.save(tag);
            }
            tagEntity = tagRepository.findOneByName(tagName);
            tagSet.add(tagEntity);
        }
        tshirt.setLink(jsonTshirt.getUrl());
        tshirt.setTags(tagSet);
        tshirt.setUser(user);
        Tshirt result = tshirtRepository.save(tshirt);
        return result != null;
    }

    public void deleteTshirt(Tshirt tshirt){
        Set<Tag> tags = tshirt.getTags();
        for(Tag tag : tags){
            Set<Tshirt> tshirts = tag.getTshirts();
            tshirts.remove(tshirt);
        }
        tshirt.setTags(null);
        tshirtRepository.delete(tshirt);
    }
}

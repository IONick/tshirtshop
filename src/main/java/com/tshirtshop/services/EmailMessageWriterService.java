package com.tshirtshop.services;

import com.tshirtshop.entities.ConfirmationToken;
import com.tshirtshop.entities.User;
import com.tshirtshop.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;

@Service
public class EmailMessageWriterService {
    @Value("${spring.mail.username")
    private String mailAddress;

    @Autowired
    private TemplateEngine templateEngine;

    public MimeMessagePreparator getConfirmMessage(User user, ConfirmationToken confirmationToken) {
        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(mailAddress);
            messageHelper.setTo(user.getEmail());
            messageHelper.setSubject("Complete Registration!\n");
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("confirmationToken", confirmationToken);
            String message = templateEngine.process("tokenmail", context);
            messageHelper.setText(message,true);
        };
    }

    public MimeMessagePreparator getBuyMessage(Order order, User user){
        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(mailAddress);
            messageHelper.setTo(user.getEmail());
            messageHelper.setSubject("thank you for buying from us!!!\n");
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("order", order);
            String message = templateEngine.process("tshirtbuy", context);
            messageHelper.setText(message,true);
        };
    }
}


package com.tshirtshop.controllers;

import com.tshirtshop.entities.Tshirt;
import com.tshirtshop.entities.User;
import com.tshirtshop.models.Role;
import com.tshirtshop.services.TshirtService;
import com.tshirtshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {
    @Autowired
    UserService userService;
    @Autowired
    TshirtService tshirtService;

    @GetMapping("/users")
    public String manageUsers(Model model){
        List<User> persons = userService.getAll();
        model.addAttribute("user" ,userService.getCurrentUser());
        model.addAttribute("persons", persons);
        return "adminpanel";
    }

    @PostMapping("/admincontrol")
    public String changeUsers(@RequestParam Map<String, String> form, @RequestParam("action") String action){
        List<User> users = userService.getAll();
        List<User> changedUsers = new ArrayList<>();
        for(User user : users){
            if(form.containsKey(user.getUsername()))
            if(form.get(user.getUsername()).equals("checked")){
                changedUsers.add(user);
            }
        }
        User currentUser = userService.getCurrentUser();
        userService.changeUsers(changedUsers, action);
        boolean exists = userService.checkUsernameExists(currentUser.getUsername());
        boolean active = currentUser.getActive();
        boolean admin = currentUser.getRole() == Role.ADMIN;
        if(!exists || !active || !admin){
            userService.logoutUser();
        }
        return "redirect:/admin/users";
    }

    @PostMapping("/deletetshirt")
    public String deleteTshirt(@ModelAttribute("tshirt") Tshirt tshirt, Model model){
        tshirtService.deleteTshirt(tshirt);
        return "redirect:/tshirts/store";
    }
}

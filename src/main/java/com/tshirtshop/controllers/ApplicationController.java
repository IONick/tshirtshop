package com.tshirtshop.controllers;

import com.tshirtshop.entities.Tshirt;
import com.tshirtshop.entities.User;
import com.tshirtshop.models.Order;
import com.tshirtshop.services.EmailMessageWriterService;
import com.tshirtshop.services.EmailSenderService;
import com.tshirtshop.services.TshirtService;
import com.tshirtshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/tshirts")
public class ApplicationController {
    @Autowired
    TshirtService tshirtService;
    @Autowired
    private UserService userService;
    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private EmailMessageWriterService messageWriterService;

    @GetMapping("/store")
    public String main(Model model){
        List<Tshirt> tshirtList = tshirtService.getAll();
        User user = userService.getCurrentUser();
        if(user == null) {
            user = new User();
        }
        model.addAttribute("user", user);
        model.addAttribute("tshirts", tshirtList);
        return "mainpage";
    }

    @GetMapping("/uploadTshirt")
    public String uploadTshirt(Model model){
        model.addAttribute("user", userService.getCurrentUser());
        model.addAttribute("tshirt", new Tshirt());
        return "uploadtshirt";
    }

    @GetMapping("/store/tshirt{tshirt}")
    public String showTshirt(@PathVariable Tshirt tshirt, Model model){
        User user = userService.getCurrentUser();
        if(user == null){
            model.addAttribute("user", new User());
            return "login";
        }
        model.addAttribute("order", new Order());
        model.addAttribute("user", user);
        tshirt.setCount(4);
        model.addAttribute("tshirt", tshirt);
        return "tshirt";
    }

    @PostMapping("/tshirt/buy")
    public String buyTshirt(@ModelAttribute Order order, @RequestParam Map<String, String> params, Model model){
        order.setName(params.get("name"));
        order.setAuthor(params.get("author"));
        order.setLink(params.get("link"));
        order.setPrice(params.get("price"));
        MimeMessagePreparator message = messageWriterService.getBuyMessage(order, userService.getCurrentUser());
        emailSenderService.sendEmail(message);
        model.addAttribute("user", userService.getCurrentUser());
        return "buy";
    }

    @GetMapping("/about")
    public String showAboutUs(Model model){
        User user = userService.getCurrentUser();
        if(user == null) user = new User();
        model.addAttribute("user", user);
        return "about";
    }


    @PostMapping("/uploadTshirt")
    public String uploadTshirt(@ModelAttribute("image")MultipartFile image, @Valid@ModelAttribute("tshirt") Tshirt tshirt,
                               Errors errors, @ModelAttribute("tagsline") String tags,  Model model) throws IOException {
        if(image.isEmpty()){
            model.addAttribute("fileError", "Please, add file of t-shirt");
            errors.reject("fileError");
        }
        if(tags == null || tags.isEmpty()){
            model.addAttribute("tagsError", "Input at least one tag");
            errors.reject("tagsError");
        }
        else if(!tshirtService.hasTags(tags)){
            model.addAttribute("tagsError", "Wrong input of tags");
            errors.reject("tagsError");

        }
        if(errors.hasErrors()){
            if(errors.hasFieldErrors("price")){
                model.addAttribute("priceError", "Wrong format of price");
            }
            model.addAttribute("user", userService.getCurrentUser());
            model.addAttribute("tshirt", tshirt);
            model.addAttribute("tagsline", tags);
            return "uploadtshirt";
        }
        User user = userService.getCurrentUser();
        boolean result = tshirtService.uploadTshirt(image, tshirt, tags, user);
        if(result) model.addAttribute("uploadSuccess", "Image successfully uploaded");
        else model.addAttribute("uploadError", "Error of upload");
        model.addAttribute("user", user);
        model.addAttribute("tshirt", new Tshirt());
        return "uploadtshirt";
    }
}

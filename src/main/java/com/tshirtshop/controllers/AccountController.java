package com.tshirtshop.controllers;

import com.tshirtshop.entities.User;
import com.tshirtshop.models.Theme;
import com.tshirtshop.validators.PasswordConfirmationValidator;
import com.tshirtshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping
public class AccountController {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordConfirmationValidator passwordConfirmationValidator;

    @GetMapping("/login")
    public String login(Model model){
        if(userService.getCurrentUser() != null){
            return "redirect:/tshirts/store";
        }
        model.addAttribute("user", new User());
        return "login";
    }

    @GetMapping("/registration")
    public String register(Model model){
        model.addAttribute("user", new User());
        return "signup";
    }


    @PostMapping("/registration")
    public String addUser(@Valid @ModelAttribute("user") User user, Errors errors,
                          @ModelAttribute("passwordConfirmation") String passwordConfirmation, Model model){
        passwordConfirmationValidator.setUser(user);
        passwordConfirmationValidator.setPasswordConfirmation(passwordConfirmation);
        if(userService.checkEmailExists(user.getEmail())){
            model.addAttribute("emailError", "That email is already registered. Please, enter another email.");
            errors.reject("emailError");
        }
        if(!passwordConfirmationValidator.isValidPasswordConfirmation()){
            model.addAttribute("passwordNotMatch","Passwords don't match");
            errors.reject("passwordConfirmationError");
        }
        boolean isNotAdded = userService.addUser(user);
        if(!isNotAdded){
            model.addAttribute("userMessage", "User with that username already exists");
            errors.reject("usernameError");
        }
        if(errors.hasErrors()){
            model.addAttribute("user", user);
            return "signup";
        }
        return "confirmation";
    }

    @GetMapping("/confirm-account")
    public String activateUser(HttpServletRequest request, Model model, @RequestParam("token") String confirmationToken, RedirectAttributes redir){
        User user = userService.activateUser(confirmationToken);
        if(user == null){
            model.addAttribute("user", new User());
            return "login";
        }
        userService.authenticateUser(user);
        return "redirect:/tshirts/store";
    }

    @GetMapping("/account/{user}")
    public String showAccount(Model model, @PathVariable User user){
        model.addAttribute("accountUser", user);
        model.addAttribute("user", userService.getCurrentUser());
        return "userpage";
    }

    @PostMapping("/theme/{user}")
    public String setTheme(@ModelAttribute("theme") String theme){
        User user = userService.getCurrentUser();
        Theme themeEnum;
        if(theme.equals("dark")) themeEnum = Theme.DARK;
        else themeEnum = Theme.LIGHT;
        user.setTheme(themeEnum);
        return "redirect:/account/" + user.getUsername();
    }
}

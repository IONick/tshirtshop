package com.tshirtshop.models;

public enum Theme {
     LIGHT, DARK
}

package com.tshirtshop.entities;

import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="tag")
public class Tag implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String name;

    @ManyToMany(mappedBy = "tags")
    private Set<Tshirt> tshirts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Tshirt> getTshirts() {
        return tshirts;
    }

    public void setTshirts(Set<Tshirt> tshirts) {
        this.tshirts = tshirts;
    }
}

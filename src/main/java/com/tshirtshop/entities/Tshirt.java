package com.tshirtshop.entities;


import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "tshirt")
public class Tshirt implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @NotEmpty(message = "Field 'name' cannot be empty")
    private String name;

    @Column(columnDefinition = "TEXT")
    @NotEmpty(message = "Field 'description' cannot be empty")
    private String description;

    @Column
    @NotEmpty(message = "Field 'theme' cannot be empty")
    private String theme;

    @Column
    private Integer count;

    @Column
    private String link;

    @Column(scale = 2)
    @NotNull(message = "Field 'price' cannot be empty")
    @DecimalMin(value = "0.0", message = "Wrong format of price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "tshirt_tag",
            joinColumns = {@JoinColumn(name = "tshirt_id")},
            inverseJoinColumns ={@JoinColumn(name = "tag_id")}
    )
    private Set<Tag> tags;

    public Tshirt() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getLink() {
        return link;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}

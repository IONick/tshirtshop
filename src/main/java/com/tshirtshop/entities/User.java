package com.tshirtshop.entities;

import com.tshirtshop.models.Role;
import com.tshirtshop.models.Theme;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "user")
public class User implements UserDetails, Serializable {
    @Id
    @NotEmpty(message = "Field 'username' cannot be empty")
    @Size(min = 4, max = 40, message = "Username must be from 4 to 40 characters")
    private String username;

    @Column
    @NotEmpty(message = "Field 'password' cannot be empty")
    @Size( min = 8, message = "Length of password must be from 8 to 40 characters")
    private String password;

    @Column(unique = true)
    @NotEmpty(message = "Field 'email' cannot be empty")
    @Email(message = "Wrong format of email")
    private String email;

    @Column
    private Boolean active;

    @Column(columnDefinition = "TINYINT UNSIGNED")
    @Enumerated(EnumType.ORDINAL)
    private Role role;

    @Column(columnDefinition  = "TINYINT UNSIGNED")
    @Enumerated(EnumType.ORDINAL)
    private Theme theme;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Tshirt> tshirts;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private ConfirmationToken confirmationToken;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Tshirt> getTshirts() {
        return tshirts;
    }

    public void setTshirts(List<Tshirt> tshirts) {
        this.tshirts = tshirts;
    }

    public ConfirmationToken getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(ConfirmationToken confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        HashSet<Role> roles = new HashSet<>();
        roles.add(getRole());
        return roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return getActive();
    }
}
